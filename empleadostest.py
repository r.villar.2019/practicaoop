#!/usr/bin/env python3

import unittest

class Empleado:
    """Un ejemplo de clase para Empleados"""
    def __init__(self, n, s):
        self.nombre = n
        self.nomina = s
        if s < 0:
            self.nomina = 0

    def calculo_impuestos (self):
        return self.nomina*0.30

    def __str__(self):
        return "El empleado {name} debe pagar {tax:.2f}".format(name=self.nombre,
 tax=self.calculo_impuestos())

class TestEmpleado(unittest.TestCase):
    def test_construir(self):
        e1 = Empleado("nombre",5000)
        self.assertEqual(e1.nomina, 5000)

    def test_impuestos(self):
        e1 = Empleado("nombre",5000)
        self.assertEqual(e1.calculo_impuestos(), 1500)

    def test_str(self):
        e1 = Empleado("pepe",50000)
        self.assertEqual("El empleado pepe debe pagar 15000.00", e1.__str__())

class Jefe(Empleado):

    def __init__(self, n, s, extra= 0):
        super().__init__(n, s)
        self.bonus = extra

    def calculo_impuestos_jefe (self):
        return super().calculo_impuestos() + self.bonus*0.30

    def __str__(self):
        return "El jefe {name} debe pagar {tax:.2f}".format(name=self.nombre,
                tax=self.calculo_impuestos_jefe())

class TestJefe(unittest.TestCase):
    def test_construir(self):
        e1 = Jefe("nombre",30000,2000)
        self.assertEqual(e1.nomina, 30000)
        self.assertEqual(e1.bonus, 2000)

    def test_impuestos(self):
        e1 = Jefe("nombre",30000,2000)
        self.assertEqual(e1.calculo_impuestos_jefe(), 9600)

    def test_str(self):
        e1 = Jefe("pepe",30000, 2000)
        self.assertEqual("El jefe pepe debe pagar 9600.00", e1.__str__())

if __name__ == "__main__":
    unittest.main()


