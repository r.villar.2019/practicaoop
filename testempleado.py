class TestEmpleado(unittest.TestCase):
    def test_construir(self):
        e1 = Empleado("nombre","nif",5000)
        self.assertEqual(e1.nomina, 5000)

    def test_impuestos(self):
        e1 = Empleado("nombre","nif",5000)
        self.assertEqual(e1.calculo_impuestos(), 1500)

    def test_str(self):
        e1 = Empleado("pepe","nif",50000)
        self.assertEqual("El empleado pepe con NIF nif debe pagar 15000.00", e1.__str__())

