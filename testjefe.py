class TestJefe(unittest.TestCase):
    def test_construir(self):
        e1 = Jefe("nombre","nif",30000,2000)
        self.assertEqual(e1.nomina, 30000)
        self.assertEqual(e1.bonus, 2000)

    def test_impuestos(self):
        e1 = Jefe("nombre","nif",30000,2000)
        self.assertEqual(e1.calculo_impuestos_jefe(), 9600)

    def test_str(self):
        e1 = Jefe("pepe","nif",30000, 2000)
        self.assertEqual("El jefe pepe con NIF nif debe pagar 9600.00", e1.__str__())

