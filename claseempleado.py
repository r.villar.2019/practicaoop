class Empleado(Persona):
 
    def __init__(self, n, d, s):
        super().__init__(n, d)
        self.nomina = s
        if s < 0:
            self.nomina = 0

    def calculo_impuestos (self):
        return self.nomina*0.30

    def __str__(self):
        return "El empleado {name} con NIF {nif} debe pagar {tax:.2f}".format(name=self.nombre,
                nif=self.nif, tax=self.calculo_impuestos())

