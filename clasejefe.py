class Jefe(Persona):
 
    def __init__(self, n, d, s, extra= 0):
        super().__init__(n, d)
        self.nomina = s
        if s < 0:
            self.nomina = 0
        self.bonus = extra

    def calculo_impuestos_jefe (self):
        return self.nomina*0.30 + self.bonus*0.30

    def __str__(self):
        return "El jefe {name} con NIF {nif} debe pagar {tax:.2f}".format(name=self.nombre,
                nif=self.nif, tax=self.calculo_impuestos_jefe())

