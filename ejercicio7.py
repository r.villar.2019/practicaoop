#!/usr/bin/env python3

import unittest

class Persona:
    def __init__(self, n, d):
        self.nombre = n
        self.nif = d

class Empleado(Persona):
 
    def __init__(self, n, d, s):
        super().__init__(n, d)
        self.nomina = s
        if s < 0:
            self.nomina = 0


    def calculo_impuestos (self):
        return self.nomina*0.30

    def __str__(self):
        return "El empleado {name} con NIF {nif} debe pagar {tax:.2f}".format(name=self.nombre,
                nif=self.nif, tax=self.calculo_impuestos())

class TestEmpleado(unittest.TestCase):
    def test_construir(self):
        e1 = Empleado("nombre","nif",5000)
        self.assertEqual(e1.nomina, 5000)

    def test_impuestos(self):
        e1 = Empleado("nombre","nif",5000)
        self.assertEqual(e1.calculo_impuestos(), 1500)

    def test_str(self):
        e1 = Empleado("pepe","nif",50000)
        self.assertEqual("El empleado pepe con NIF nif debe pagar 15000.00", e1.__str__())

class Jefe(Persona):
 
    def __init__(self, n, d, s, extra= 0):
        super().__init__(n, d)
        self.nomina = s
        if s < 0:
            self.nomina = 0
        self.bonus = extra

    def calculo_impuestos_jefe (self):
        return self.nomina*0.30 + self.bonus*0.30

    def __str__(self):
        return "El jefe {name} con NIF {nif} debe pagar {tax:.2f}".format(name=self.nombre,
                nif=self.nif, tax=self.calculo_impuestos_jefe())

class TestJefe(unittest.TestCase):
    def test_construir(self):
        e1 = Jefe("nombre","nif",30000,2000)
        self.assertEqual(e1.nomina, 30000)
        self.assertEqual(e1.bonus, 2000)

    def test_impuestos(self):
        e1 = Jefe("nombre","nif",30000,2000)
        self.assertEqual(e1.calculo_impuestos_jefe(), 9600)

    def test_str(self):
        e1 = Jefe("pepe","nif",30000, 2000)
        self.assertEqual("El jefe pepe con NIF nif debe pagar 9600.00", e1.__str__())

if __name__ == "__main__":
    unittest.main()

